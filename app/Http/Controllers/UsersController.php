<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\UserCollection;

use App\User;

class UsersController extends Controller
{
    public function list (){
        //Cache::forget('users');

        if(!Cache::has('users')) {
            $users = new UserCollection(User::all());

            Cache::remember('users', 60 * 60, function () use ($users) {
                return User::all()->push($users);
            });
        } else {
            $users = Cache::get('users');
            //dd($users);
        }

        return $users;
    }
}
