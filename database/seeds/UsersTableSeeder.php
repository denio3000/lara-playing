<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    protected $faker;

    public function __construct(Faker\Generator $faker) {
        $this->faker = $faker;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (range(1, 10) as $index) {
            User::insert([
                "name" => $this->faker->name,
                "email" => $this->faker->email,
                "password" => bcrypt("pepsi"),
            ]);
        }
    }
}
